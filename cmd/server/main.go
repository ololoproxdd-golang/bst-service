package main

import (
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/server"
)

func main() {
	srv := server.NewServer(config.LoadConfig())
	srv.Run()
}
