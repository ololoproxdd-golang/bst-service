module gitlab.com/ololoproxdd-golang/bst-service

go 1.14

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/go-kit/kit v0.10.0
	github.com/go-testfixtures/testfixtures/v3 v3.3.0
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/golang/mock v1.4.3
	github.com/gorilla/mux v1.7.4
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jessevdk/go-flags v1.4.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/julienschmidt/httprouter v1.2.0 // indirect
	github.com/nsf/jsondiff v0.0.0-20200515183724-f29ed568f4ce
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/rs/zerolog v1.19.0
	github.com/shopspring/decimal v1.2.0 // indirect
)
