package test

import (
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/middleware"
	"strconv"
	"time"
)

type testServer struct {
	config config.Config
	db     *sqlx.DB
}

func NewTestServer(config config.Config) *testServer {
	return &testServer{config: config}
}

func init() {
	time.Local = time.UTC
}

func (s *testServer) GetTestDbConnection() *sqlx.DB {
	if s.db != nil {
		return s.db
	}

	conf := s.getTestDbConfig()
	conf.RuntimeParams = map[string]string{"timezone": "UTC"}
	conf.LogLevel = pgx.LogLevelError
	nativeDb := stdlib.OpenDB(conf)
	sqlxConnection := sqlx.NewDb(nativeDb, "pgx")
	sqlxConnection.SetConnMaxLifetime(0)
	sqlxConnection.SetMaxOpenConns(10)
	sqlxConnection.SetMaxIdleConns(10)

	if err := sqlxConnection.Ping(); err != nil {
		log.Fatal().Err(err).Msg("test DB postgres connection fail")
	}

	s.db = sqlxConnection
	return s.db
}

func (s *testServer) getTestDbConfig() pgx.ConnConfig {
	port, err := strconv.ParseUint(s.config.PgPort, 10, 16)
	if err != nil {
		log.Fatal().Err(err).Msg("postgres wrong port")
	}

	return pgx.ConnConfig{
		Host:     s.config.PgHost,
		Port:     uint16(port),
		Database: s.config.PgDb,
		User:     s.config.PgUser,
		Password: s.config.PgPass,

		PreferSimpleProtocol: true,

		Logger: middleware.NewPgxLogger(),
	}
}
