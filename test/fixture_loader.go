package test

import (
	"database/sql"
	"errors"
	"github.com/go-testfixtures/testfixtures/v3"
	_ "github.com/jackc/pgx"
	"io/ioutil"
	"path"
	"runtime"
	"strings"
)

type FixtureLoader struct {
	db       *sql.DB
	fixtures *testfixtures.Loader
}

func LoadFixtures(db *sql.DB) error {
	loader := &FixtureLoader{db: db}
	return loader.loadPostgres()
}

func (fl *FixtureLoader) loadPostgres() (err error) {
	currentDir, err := fl.currentDir()
	if err != nil {
		return err
	}

	if fl.fixtures == nil {
		schema := currentDir + "/schema.sql"
		if err = fl.loadSchema(fl.db, schema, ";\r"); err != nil {
			return err
		}

		dir := currentDir + "/fixtures"
		fl.fixtures, err = fl.initFixtures(fl.db, "postgres", dir)
		if err != nil {
			return err
		}
	}

	return fl.fixtures.Load()
}

func (fl *FixtureLoader) loadSchema(db *sql.DB, schema, sep string) error {
	file, err := ioutil.ReadFile(schema)
	if err != nil {
		return err
	}

	requests := strings.Split(string(file), sep)
	for _, request := range requests {
		if _, err := db.Exec(request); err != nil {
			return err
		}
	}

	return nil
}

func (fl *FixtureLoader) initFixtures(db *sql.DB, helper, dir string) (*testfixtures.Loader, error) {
	return testfixtures.New(
		testfixtures.Database(db),
		testfixtures.Dialect(helper),
		testfixtures.Directory(dir),
		testfixtures.DangerousSkipTestDatabaseCheck(),
	)
}

func (fl *FixtureLoader) currentDir() (string, error) {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return "", errors.New("No caller information ")
	}
	return path.Dir(filename), nil
}
