drop schema if exists structure cascade;

create schema structure;
alter schema structure owner to postgres;

create table structure.tree
(
	id bigserial not null
		constraint tree_pk
			primary key,
	value jsonb not null
);
alter table structure.tree owner to postgres;

