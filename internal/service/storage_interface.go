package service

import (
	"context"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
)

type HealthStorage interface {
	Check(ctx context.Context) error
}
type HealthStorageMiddleware func(HealthStorage) HealthStorage

type TreeStorage interface {
	Create(ctx context.Context, tree model.Tree) (int64, error)
	Read(ctx context.Context, id int64) (model.Tree, error)
	Update(ctx context.Context, tree model.Tree) error
}
type TreeStorageMiddleware func(TreeStorage) TreeStorage
