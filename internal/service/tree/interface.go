package tree

import (
	"context"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
)

type Service interface {
	Create(ctx context.Context, req model.CreateRequest) (model.Tree, error)
	Read(ctx context.Context, req model.ReadRequest) (model.Tree, bool, error)
	InsertValue(ctx context.Context, req model.InsertRequest) (model.Tree, error)
	DeleteValue(ctx context.Context, req model.DeleteRequest) (model.Tree, bool, error)
}
type ServiceMiddleware func(Service) Service
