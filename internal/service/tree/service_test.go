package tree

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	"gitlab.com/ololoproxdd-golang/bst-service/test/mock"
	"reflect"
	"testing"
)

func TestUnit_Service_Create(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := config.LoadConfig()
	store := mock.NewMockTreeStorage(ctrl)
	svc := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(store))

	var flagTests = []struct {
		testName   string
		ctx        context.Context
		request    model.CreateRequest
		mockAction func(context.Context, error)
		expResp    model.Tree
		mockErr    error
		expErr     error
	}{
		{
			testName: "error request values tree",
			ctx:      context.Background(),
			request: model.CreateRequest{
				Values: nil,
			},
			mockAction: func(ctx context.Context, mockErr error) {},
			expResp:    model.Tree{},
			mockErr:    nil,
			expErr:     errEmptyValues,
		},
		{
			testName: "error create tree in store",
			ctx:      context.Background(),
			request: model.CreateRequest{
				Values: []int64{1, 2, 3, 5, 6},
			},
			mockAction: func(ctx context.Context, mockErr error) {
				var id int64
				store.EXPECT().Create(ctx, gomock.AssignableToTypeOf(model.Tree{})).Return(id, mockErr)
			},
			expResp: model.Tree{},
			mockErr: errors.New("error create tree in store"),
			expErr:  errCreateTree,
		},
		{
			testName: "successful create tree",
			ctx:      context.Background(),
			request: model.CreateRequest{
				Values: []int64{10, 20, 30},
			},
			mockAction: func(ctx context.Context, mockErr error) {
				id := int64(100)
				store.EXPECT().Create(ctx, gomock.AssignableToTypeOf(model.Tree{})).Return(id, mockErr)
			},
			expResp: model.Tree{
				ID: 100,
				Head: &model.Node{
					Value:  20,
					Height: 1,
					Left: &model.Node{
						Value: 10,
					},
					Right: &model.Node{
						Value: 30,
					},
				},
			},
			mockErr: nil,
			expErr:  nil,
		},
	}

	var resp model.Tree
	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			tt.mockAction(tt.ctx, tt.mockErr)

			resp, err = svc.Create(tt.ctx, tt.request)
			if !reflect.DeepEqual(resp, tt.expResp) {
				t.Fatal("resp != rr.expResp")
			}
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}

func TestUnit_Service_Read(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := config.LoadConfig()
	store := mock.NewMockTreeStorage(ctrl)
	svc := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(store))

	var flagTests = []struct {
		testName   string
		ctx        context.Context
		request    model.ReadRequest
		mockAction func(context.Context, int64, model.Tree, error)
		mockTree   model.Tree
		mockErr    error
		expResp    model.Tree
		expFound   bool
		expErr     error
	}{
		{
			testName: "request id error",
			ctx:      context.Background(),
			request: model.ReadRequest{
				IDRequest: model.IDRequest{ID: -100},
				Value:     nil,
			},
			mockAction: func(context.Context, int64, model.Tree, error) {},
			mockTree:   model.Tree{},
			mockErr:    nil,
			expResp:    model.Tree{},
			expFound:   false,
			expErr:     errInvalidTreeID,
		},
		{
			testName: "read tree from store error",
			ctx:      context.Background(),
			request: model.ReadRequest{
				IDRequest: model.IDRequest{ID: 123},
				Value:     nil,
			},
			mockAction: func(ctx context.Context, id int64, mockTree model.Tree, mockErr error) {
				store.EXPECT().Read(ctx, id).Return(mockTree, mockErr)
			},
			mockTree: model.Tree{},
			mockErr:  errors.New("read tree from store error"),
			expResp:  model.Tree{},
			expFound: false,
			expErr:   errReadTree,
		},
		{
			testName: "successful read tree (without search tree value)",
			ctx:      context.Background(),
			request: model.ReadRequest{
				IDRequest: model.IDRequest{ID: 456},
				Value:     nil,
			},
			mockAction: func(ctx context.Context, id int64, mockTree model.Tree, mockErr error) {
				store.EXPECT().Read(ctx, id).Return(mockTree, mockErr)
			},
			mockTree: model.Tree{
				ID: 456,
				Head: &model.Node{
					Value:  10,
					Height: 1,
					Left: &model.Node{
						Value: 1,
					},
					Right: &model.Node{
						Value: 100,
					},
				},
			},
			mockErr: nil,
			expResp: model.Tree{
				ID: 456,
				Head: &model.Node{
					Value:  10,
					Height: 1,
					Left: &model.Node{
						Value: 1,
					},
					Right: &model.Node{
						Value: 100,
					},
				},
			},
			expFound: false,
			expErr:   nil,
		},
	}

	var resp model.Tree
	var found bool
	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			tt.mockAction(tt.ctx, tt.request.ID, tt.mockTree, tt.mockErr)

			resp, found, err = svc.Read(tt.ctx, tt.request)
			if !reflect.DeepEqual(resp, tt.expResp) {
				t.Fatal("resp != rr.expResp")
			}
			if found != tt.expFound {
				t.Fatal("found != tt.expFound")
			}
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}

func TestUnit_Service_InsertValue(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := config.LoadConfig()
	store := mock.NewMockTreeStorage(ctrl)
	svc := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(store))

	var flagTests = []struct {
		testName   string
		ctx        context.Context
		request    model.InsertRequest
		mockAction func(context.Context, int64, model.Tree, model.Tree, error)
		mockTree   model.Tree
		mockErr    error
		expResp    model.Tree
		expErr     error
	}{
		{
			testName: "request id error",
			ctx:      context.Background(),
			request: model.InsertRequest{
				IDRequest:    model.IDRequest{ID: -999},
				ValueRequest: model.ValueRequest{Value: 123},
			},
			mockAction: func(context.Context, int64, model.Tree, model.Tree, error) {},
			mockTree:   model.Tree{},
			mockErr:    nil,
			expResp:    model.Tree{},
			expErr:     errInvalidTreeID,
		},
		{
			testName: "read tree error",
			ctx:      context.Background(),
			request: model.InsertRequest{
				IDRequest:    model.IDRequest{ID: 1990},
				ValueRequest: model.ValueRequest{Value: 123},
			},
			mockAction: func(ctx context.Context, id int64, _ model.Tree, mockTree model.Tree, mockErr error) {
				store.EXPECT().Read(ctx, id).Return(mockTree, mockErr)
			},
			mockTree: model.Tree{},
			mockErr:  errors.New("read tree from store error"),
			expResp:  model.Tree{},
			expErr:   errReadTree,
		},
		{
			testName: "successful add value to tree",
			ctx:      context.Background(),
			request: model.InsertRequest{
				IDRequest:    model.IDRequest{ID: 456},
				ValueRequest: model.ValueRequest{Value: 123},
			},
			mockAction: func(ctx context.Context, id int64, tree model.Tree, mockTree model.Tree, mockErr error) {
				store.EXPECT().Read(ctx, id).Return(mockTree, nil)
				store.EXPECT().Update(ctx, tree).Return(nil)
			},
			mockTree: model.Tree{
				ID: 456,
				Head: &model.Node{
					Value:  20,
					Height: 1,
					Left: &model.Node{
						Value: 10,
					},
					Right: &model.Node{
						Value: 30,
					},
				},
			},
			mockErr: nil,
			expResp: model.Tree{
				ID: 456,
				Head: &model.Node{
					Value:  20,
					Height: 2,
					Left: &model.Node{
						Value: 10,
					},
					Right: &model.Node{
						Value:  30,
						Height: 1,
						Right: &model.Node{
							Value: 123,
						},
					},
				},
			},
			expErr: nil,
		},
	}

	var resp model.Tree
	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			tt.mockAction(tt.ctx, tt.request.ID, tt.expResp, tt.mockTree, tt.mockErr)

			resp, err = svc.InsertValue(tt.ctx, tt.request)
			if !reflect.DeepEqual(resp, tt.expResp) {
				t.Fatal("resp != rr.expResp")
			}
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}

func TestUnit_Service_DeleteValue(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := config.LoadConfig()
	store := mock.NewMockTreeStorage(ctrl)
	svc := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(store))

	var flagTests = []struct {
		testName   string
		ctx        context.Context
		request    model.DeleteRequest
		mockAction func(context.Context, int64, model.Tree, model.Tree, error)
		mockTree   model.Tree
		mockErr    error
		expResp    model.Tree
		expDeleted bool
		expErr     error
	}{
		{
			testName: "request id error",
			ctx:      context.Background(),
			request: model.DeleteRequest{
				IDRequest:    model.IDRequest{ID: -999},
				ValueRequest: model.ValueRequest{Value: 123},
			},
			mockAction: func(context.Context, int64, model.Tree, model.Tree, error) {},
			mockTree:   model.Tree{},
			mockErr:    nil,
			expResp:    model.Tree{},
			expDeleted: false,
			expErr:     errInvalidTreeID,
		},
		{
			testName: "read tree error",
			ctx:      context.Background(),
			request: model.DeleteRequest{
				IDRequest:    model.IDRequest{ID: 1990},
				ValueRequest: model.ValueRequest{Value: 123},
			},
			mockAction: func(ctx context.Context, id int64, _ model.Tree, mockTree model.Tree, mockErr error) {
				store.EXPECT().Read(ctx, id).Return(mockTree, mockErr)
			},
			mockTree:   model.Tree{},
			mockErr:    errors.New("read tree from store error"),
			expResp:    model.Tree{},
			expDeleted: false,
			expErr:     errReadTree,
		},
		{
			testName: "successful delete value from tree",
			ctx:      context.Background(),
			request: model.DeleteRequest{
				IDRequest:    model.IDRequest{ID: 456},
				ValueRequest: model.ValueRequest{Value: 123},
			},
			mockAction: func(ctx context.Context, id int64, tree model.Tree, mockTree model.Tree, mockErr error) {
				store.EXPECT().Read(ctx, id).Return(mockTree, nil)
				store.EXPECT().Update(ctx, tree).Return(nil)
			},
			mockTree: model.Tree{
				ID: 456,
				Head: &model.Node{
					Value:  20,
					Height: 2,
					Left: &model.Node{
						Value: 10,
					},
					Right: &model.Node{
						Value:  30,
						Height: 1,
						Right: &model.Node{
							Value: 123,
						},
					},
				},
			},
			mockErr: nil,
			expResp: model.Tree{
				ID: 456,
				Head: &model.Node{
					Value:  20,
					Height: 1,
					Left: &model.Node{
						Value: 10,
					},
					Right: &model.Node{
						Value: 30,
					},
				},
			},
			expDeleted: true,
			expErr:     nil,
		},
	}

	var resp model.Tree
	var deleted bool
	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			tt.mockAction(tt.ctx, tt.request.ID, tt.expResp, tt.mockTree, tt.mockErr)

			resp, deleted, err = svc.DeleteValue(tt.ctx, tt.request)
			if !reflect.DeepEqual(resp, tt.expResp) {
				t.Fatal("resp != rr.expResp")
			}
			if deleted != tt.expDeleted {
				t.Fatal("deleted != tt.expDeleted")
			}
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}
