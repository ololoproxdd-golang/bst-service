package tree

import (
	"context"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
)

var (
	errInvalidTreeID = errors.New("invalid tree id error")
	errEmptyValues   = errors.New("input values is empty")
	errCreateTree    = errors.New("create tree error")
	errReadTree      = errors.New("read tree error")
	errUpdateTree    = errors.New("update tree error")
)

type service struct {
	store svc.TreeStorage
}

func New(store svc.TreeStorage) Service {
	return service{store}
}

func (s service) Create(ctx context.Context, req model.CreateRequest) (model.Tree, error) {
	if len(req.Values) == 0 {
		log.Error().Err(errEmptyValues)
		return model.Tree{}, errEmptyValues
	}

	tree := model.Tree{}
	tree.NewBst(req.Values)

	var err error
	tree.ID, err = s.store.Create(ctx, tree)
	if err != nil {
		log.Error().Err(err).Msg(errCreateTree.Error())
		return model.Tree{}, errCreateTree
	}

	return tree, nil
}

func (s service) Read(ctx context.Context, req model.ReadRequest) (model.Tree, bool, error) {
	tree, found, err := s.get(ctx, req.ID)
	if !found || err != nil {
		return model.Tree{}, false, err
	}

	return tree, tree.Search(req.Value), nil
}

func (s service) InsertValue(ctx context.Context, req model.InsertRequest) (model.Tree, error) {
	tree, found, err := s.get(ctx, req.ID)
	if !found || err != nil {
		return model.Tree{}, err
	}

	tree.Insert(req.Value)

	err = s.store.Update(ctx, tree)
	if err != nil {
		log.Error().Err(err).Msg(errUpdateTree.Error())
		return model.Tree{}, errUpdateTree
	}

	return tree, nil
}

func (s service) DeleteValue(ctx context.Context, req model.DeleteRequest) (model.Tree, bool, error) {
	tree, found, err := s.get(ctx, req.ID)
	if !found || err != nil {
		return model.Tree{}, false, err
	}

	deleted := tree.Delete(req.Value)
	if deleted {
		err = s.store.Update(ctx, tree)
		if err != nil {
			log.Error().Err(err).Msg(errUpdateTree.Error())
			return model.Tree{}, false, errUpdateTree
		}
	}

	return tree, deleted, nil
}

func (s service) get(ctx context.Context, id int64) (model.Tree, bool, error) {
	if id <= 0 {
		log.Error().Err(errInvalidTreeID)
		return model.Tree{}, false, errInvalidTreeID
	}

	tree, err := s.store.Read(ctx, id)
	if err != nil {
		log.Error().Err(err).Msg(errReadTree.Error())
		return model.Tree{}, false, errReadTree
	}
	if tree.ID == 0 {
		return model.Tree{}, false, nil
	}

	return tree, true, nil
}
