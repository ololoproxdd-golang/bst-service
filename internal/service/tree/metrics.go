package tree

import (
	"context"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	"time"
)

const (
	methodCreate      = "service/tree/Create"
	methodRead        = "service/tree/Read"
	methodInsertValue = "service/tree/InsertValue"
	methodDeleteValue = "service/tree/DeleteValue"
)

type metricMiddleware struct {
	Service
	monitor *metrics.Monitor
}

func NewMetrics(monitor *metrics.Monitor) ServiceMiddleware {
	return func(next Service) Service {
		return &metricMiddleware{
			Service: next,
			monitor: monitor,
		}
	}
}

func (m *metricMiddleware) Create(ctx context.Context, req model.CreateRequest) (model.Tree, error) {
	defer m.monitor.Metrics.DurationInc(methodCreate, time.Now())
	r, err := m.Service.Create(ctx, req)
	m.monitor.Metrics.CountInc(methodCreate, err)
	return r, err
}

func (m *metricMiddleware) Read(ctx context.Context, req model.ReadRequest) (model.Tree, bool, error) {
	defer m.monitor.Metrics.DurationInc(methodRead, time.Now())
	r, f, err := m.Service.Read(ctx, req)
	m.monitor.Metrics.CountInc(methodRead, err)
	return r, f, err
}

func (m *metricMiddleware) InsertValue(ctx context.Context, req model.InsertRequest) (model.Tree, error) {
	defer m.monitor.Metrics.DurationInc(methodInsertValue, time.Now())
	r, err := m.Service.InsertValue(ctx, req)
	m.monitor.Metrics.CountInc(methodInsertValue, err)
	return r, err
}

func (m *metricMiddleware) DeleteValue(ctx context.Context, req model.DeleteRequest) (model.Tree, bool, error) {
	defer m.monitor.Metrics.DurationInc(methodDeleteValue, time.Now())
	r, d, err := m.Service.DeleteValue(ctx, req)
	m.monitor.Metrics.CountInc(methodDeleteValue, err)
	return r, d, err
}
