package health

import "context"

type Service interface {
	Check(ctx context.Context) (string, error)
	Ping(ctx context.Context) string
}
type ServiceMiddleware func(Service) Service
