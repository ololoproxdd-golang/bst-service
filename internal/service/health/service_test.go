package health

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	"gitlab.com/ololoproxdd-golang/bst-service/test/mock"
	"reflect"
	"testing"
)

func TestUnit_Service_Ping(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := config.LoadConfig()
	store := mock.NewMockHealthStorage(ctrl)
	svc := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(store))

	var flagTests = []struct {
		testName string
		ctx      context.Context
		expResp  string
	}{
		{
			testName: "ping pong successful",
			ctx:      context.Background(),
			expResp:  pong,
		},
	}
	var resp string
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			resp = svc.Ping(tt.ctx)
			if !reflect.DeepEqual(resp, tt.expResp) {
				t.Fatal("resp != rr.expResp")
			}
		})
	}
}

func TestUnit_Service_Check(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	c := config.LoadConfig()
	store := mock.NewMockHealthStorage(ctrl)
	svc := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(store))

	var flagTests = []struct {
		testName   string
		ctx        context.Context
		mockAction func(context.Context, error)
		expResp    string
		mockErr    error
		expErr     error
	}{
		{
			testName: "check store error",
			ctx:      context.Background(),
			mockAction: func(ctx context.Context, mockErr error) {
				store.EXPECT().Check(ctx).Return(mockErr)
			},
			expResp: checkFail,
			mockErr: errors.New("check store error"),
			expErr:  errCheckStorage,
		},
		{
			testName: "check store successful",
			ctx:      context.Background(),
			mockAction: func(ctx context.Context, mockErr error) {
				store.EXPECT().Check(ctx).Return(mockErr)
			},
			expResp: checkOk,
			mockErr: nil,
			expErr:  nil,
		},
	}

	var resp string
	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			tt.mockAction(tt.ctx, tt.mockErr)

			resp, err = svc.Check(tt.ctx)
			if !reflect.DeepEqual(resp, tt.expResp) {
				t.Fatal("resp != rr.expResp")
			}
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}
