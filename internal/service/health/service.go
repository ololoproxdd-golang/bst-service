package health

import (
	"context"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
)

const (
	pong      = "Pong"
	checkOk   = "Ok"
	checkFail = "Fail"
)

var (
	errCheckStorage = errors.New("health check storage error")
)

type service struct {
	storage svc.HealthStorage
}

func New(storage svc.HealthStorage) Service {
	return &service{storage}
}

func (s service) Ping(context.Context) string {
	return pong
}

func (s service) Check(ctx context.Context) (string, error) {
	err := s.storage.Check(ctx)
	if err != nil {
		log.Error().Err(err).Msg(errCheckStorage.Error())
		return checkFail, errCheckStorage
	}

	return checkOk, nil
}
