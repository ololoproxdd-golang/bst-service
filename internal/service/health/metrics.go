package health

import (
	"context"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	"time"
)

const (
	methodCheck = "service/health/Check"
	methodPing  = "service/health/Ping"
)

type metricMiddleware struct {
	Service
	monitor *metrics.Monitor
}

func NewMetrics(monitor *metrics.Monitor) ServiceMiddleware {
	return func(next Service) Service {
		return &metricMiddleware{
			Service: next,
			monitor: monitor,
		}
	}
}

func (m *metricMiddleware) Check(ctx context.Context) (string, error) {
	defer m.monitor.Metrics.DurationInc(methodCheck, time.Now())
	r, err := m.Service.Check(ctx)
	m.monitor.Metrics.CountInc(methodCheck, err)
	return r, err
}

func (m *metricMiddleware) Ping(ctx context.Context) string {
	defer m.monitor.Metrics.DurationInc(methodPing, time.Now())
	r := m.Service.Ping(ctx)
	m.monitor.Metrics.CountInc(methodPing, nil)
	return r
}
