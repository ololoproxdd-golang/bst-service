package transport

type ReadResponse struct {
	Tree    Tree `json:"tree"`
	IsFound bool `json:"is_found"`
}

type DeleteResponse struct {
	Tree      Tree `json:"tree"`
	IsDeleted bool `json:"is_deleted"`
}
