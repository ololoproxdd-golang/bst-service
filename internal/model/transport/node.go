package transport

import model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"

type Node struct {
	Value int64 `json:"value"`
	Left  *Node `json:"left,omitempty"`
	Right *Node `json:"right,omitempty"`
}

func (node Node) ToTransportModel(n *model.Node) *Node {
	if n == nil {
		return nil
	}

	node.Value = n.Value
	node.Left = Node{}.ToTransportModel(n.Left)
	node.Right = Node{}.ToTransportModel(n.Right)

	return &node
}
