package transport

import model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"

type Tree struct {
	ID   int64 `json:"id"`
	Head *Node `json:"data"`
}

func (tree Tree) ToTransportModel(t model.Tree) Tree {
	tree.ID = t.ID
	tree.Head = Node{}.ToTransportModel(t.Head)
	return tree
}
