package service

type IDRequest struct {
	ID int64
}

type ValueRequest struct {
	Value int64
}

type IDValueRequest struct {
	IDRequest
	ValueRequest
}

type CreateRequest struct {
	Values []int64
}

type ReadRequest struct {
	IDRequest
	Value *int64
}

type InsertRequest IDValueRequest

type DeleteRequest IDValueRequest
