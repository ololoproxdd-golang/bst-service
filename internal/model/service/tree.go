package service

import (
	"sort"
)

type Tree struct {
	ID   int64
	Head *Node
}

func (t *Tree) NewBst(values []int64) *Tree {
	sort.Slice(values, func(i, j int) bool {
		return values[i] < values[j]
	})

	count := int64(len(values)) - 1
	t.Head, _ = (&Node{}).buildBst(values, 0, count)

	return t
}

func (t *Tree) Search(value *int64) (found bool) {
	if value == nil {
		return false
	}

	return t.Head.search(*value)
}

func (t *Tree) Insert(v int64) {
	t.Head = t.Head.insert(v)
}

func (t *Tree) Delete(v int64) bool {
	var deleted bool
	t.Head, _, deleted = t.Head.remove(v)

	return deleted
}
