package service

type Node struct {
	Value  int64
	Left   *Node
	Right  *Node
	Height int64
}

func (node *Node) buildBst(values []int64, start, end int64) (*Node, int64) {
	if end < start {
		return nil, 0
	}

	mid := (start + end) / 2
	node.Value = values[mid]

	var hl, hr int64
	node.Left, hl = (&Node{}).buildBst(values, start, mid-1)
	if node.Left != nil {
		node.Left.Height = hl
		hl++
	}

	node.Right, hr = (&Node{}).buildBst(values, mid+1, end)
	if node.Right != nil {
		node.Right.Height = hr
		hr++
	}

	max := hl
	if hr > hl {
		max = hr
	}
	node.Height = max

	return node, max
}

func (node *Node) search(value int64) bool {
	if node == nil {
		return false
	}

	if node.Value == value {
		return true
	}

	if node.Value < value {
		return node.Right.search(value)
	}

	if node.Value > value {
		return node.Left.search(value)
	}

	return false
}

func (node *Node) height() int64 {
	if node == nil {
		return 0
	}

	return node.Height
}

func (node *Node) bFactor() int64 {
	return node.Right.height() - node.Left.height()
}

func (node *Node) fixHeight() {
	if node.Left == nil && node.Right == nil {
		node.Height = 0
		return
	}

	hl := node.Left.height()
	hr := node.Right.height()

	max := hr
	if hl > hr {
		max = hl
	}

	node.Height = max + 1
}

func (node *Node) rotateRight() *Node {
	l := node.Left
	node.Left = l.Right
	l.Right = node

	node.fixHeight()
	l.fixHeight()

	return l
}

func (node *Node) rotateLeft() *Node {
	r := node.Right
	node.Right = r.Left
	r.Left = node

	node.fixHeight()
	r.fixHeight()

	return r
}

func (node *Node) balance() *Node {
	node.fixHeight()

	if node.bFactor() == 2 {
		if node.Right.bFactor() < 0 {
			node.Right = node.Right.rotateRight()
		}

		return node.rotateLeft()
	}

	if node.bFactor() == -2 {
		if node.Left.bFactor() > 0 {
			node.Left = node.Left.rotateLeft()
		}

		return node.rotateRight()
	}

	return node
}

func (node *Node) insert(v int64) *Node {
	if node == nil {
		return &Node{
			Value: v,
		}
	}

	if v < node.Value {
		node.Left = node.Left.insert(v)
	} else {
		node.Right = node.Right.insert(v)
	}

	return node.balance()
}

func (node *Node) findMin() *Node {
	if node.Left == nil {
		return node
	}

	return node.Left.findMin()
}

func (node *Node) removeMin() *Node {
	if node.Left.height() == 0 {
		return node.Right
	}

	node.Left = node.Left.removeMin()

	return node.balance()
}

func (node *Node) remove(v int64) (*Node, bool, bool) {
	if node == nil {
		return nil, false, false
	}

	var upd, del bool
	switch {
	case v < node.Value:
		node.Left, del, upd = node.Left.remove(v)
		if del {
			node.Left = nil
		}

		return node.balance(), false, upd

	case v > node.Value:
		node.Right, del, upd = node.Right.remove(v)
		if del {
			node.Right = nil
		}

		return node.balance(), false, upd

	default:
		l := node.Left
		r := node.Right

		if r == nil {
			return l, true, true
		}

		min := r.findMin()
		min.Right = r.removeMin()
		min.Left = l

		return min.balance(), true, true
	}
}
