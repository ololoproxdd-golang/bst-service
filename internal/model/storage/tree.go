package storage

import model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"

type Tree struct {
	ID    int64  `db:"id"`
	Value string `db:"value"`
	Head  *Node
}

func (tree Tree) ToStorageModel(t model.Tree) Tree {
	tree.ID = t.ID
	tree.Head = Node{}.ToStorageModel(t.Head)
	return tree
}

func (tree Tree) ToServiceModel() model.Tree {
	return model.Tree{
		ID:   tree.ID,
		Head: tree.Head.ToServiceModel(),
	}
}
