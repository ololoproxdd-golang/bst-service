package storage

import model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"

type Node struct {
	Value  int64 `json:"v"`
	Left   *Node `json:"l"`
	Right  *Node `json:"r"`
	Height int64 `json:"h"`
}

func (node Node) ToStorageModel(n *model.Node) *Node {
	if n == nil {
		return nil
	}

	node.Value = n.Value
	node.Height = n.Height
	node.Left = Node{}.ToStorageModel(n.Left)
	node.Right = Node{}.ToStorageModel(n.Right)

	return &node
}

func (node *Node) ToServiceModel() *model.Node {
	if node == nil {
		return nil
	}

	return &model.Node{
		Value:  node.Value,
		Height: node.Height,
		Left:   node.Left.ToServiceModel(),
		Right:  node.Right.ToServiceModel(),
	}
}
