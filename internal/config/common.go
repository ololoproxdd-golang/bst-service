package config

import (
	"github.com/jessevdk/go-flags"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	"os"
)

type Config struct {
	AppEnv      string `short:"e" long:"app-env" env:"APP_ENV" description:"Application environment"`
	AppName     string `short:"n" long:"app-name" env:"APP_NAME" description:"Application name"`
	AppHost     string `short:"h" long:"app-host" env:"APP_HOST" description:"Application server host"`
	AppHttpPort string `short:"g" long:"app-http-port" env:"APP_HTTP_PORT" description:"Gateway http/rest port"`
	LogPath     string `short:"l" long:"log-path" env:"LOG_PATH" description:"Filename for logs"`

	PgHost string `long:"pg-host" env:"POSTGRES_HOST" description:"PostgreSQL host name"`
	PgPort string `long:"pg-port" env:"POSTGRES_PORT" description:"PostgreSQL port"`
	PgUser string `long:"pg-user" env:"POSTGRES_USER" description:"PostgreSQL user name"`
	PgPass string `long:"pg-pass" env:"POSTGRES_PASSWORD" description:"PostgreSQL password"`
	PgDb   string `long:"pg-db" env:"POSTGRES_DB_NAME" description:"PostgreSQL db name"`
	PgSsl  string `long:"pg-ssl" env:"POSTGRES_SSL_MODE" description:"PostgreSQL ssl mode"`

	HostName string

	Test string `short:"t" long:"is_test"`
}

func LoadConfig() Config {
	var config Config
	var err error

	_ = godotenv.Load()
	parser := flags.NewParser(&config, flags.Default)
	if _, err = parser.Parse(); err != nil {
		log.Fatal().Err(err).Msg("cant parse env")
	}

	config.HostName = getHostName()

	return config
}

func getHostName() string {
	hostName, err := os.Hostname()
	if err != nil {
		log.Warn().Err(err).Msg("cant get hostname")
	}

	return hostName
}
