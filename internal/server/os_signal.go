package server

import (
	"github.com/rs/zerolog/log"
	"os"
	"os/signal"
	"syscall"
)

func (s *server) registerOsSignal() {
	osSignal := make(chan os.Signal, 1)
	signal.Notify(osSignal, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		defer close(osSignal)
		sig := <-osSignal
		log.Info().Msgf("get signal: %s", sig)
		s.Close()
	}()
}
