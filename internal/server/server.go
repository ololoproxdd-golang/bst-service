package server

import (
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/service/tree"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/transport"
	"net/http"
	"sync"
	"sync/atomic"
	"time"
)

const (
	stateNew int32 = iota + 1
	stateRun
	stateClosed

	closeTimeout = 5
)

type server struct {
	config config.Config
	state  int32

	endpoints  *transport.Endpoints
	httpServer *http.Server

	monitorCollector *metrics.Monitor

	service struct {
		health health.Service
		tree   tree.Service
	}

	storage struct {
		health svc.HealthStorage
		tree   svc.TreeStorage
	}

	db *sqlx.DB
}

func NewServer(config config.Config) *server {
	s := &server{config: config, state: stateNew}
	s.initLogger()

	return s
}

func (s *server) Run() {
	if !atomic.CompareAndSwapInt32(&s.state, stateNew, stateRun) {
		log.Error().Msg("cant run server: server is not opened")
		return
	}

	log.Info().Msg("running bst-service")
	s.registerOsSignal()

	wg := sync.WaitGroup{}
	wg.Add(1)
	go s.runHttpServer(&wg)

	wg.Wait()
	log.Info().Msg("bst-service stopped ...")
}

func (s *server) runHttpServer(wait *sync.WaitGroup) {
	defer wait.Done()
	log.Info().Msgf("running http server address: %s ...", s.getTransportHttp().Addr)

	err := s.getTransportHttp().ListenAndServe()
	log.Info().Err(err).Msg("closed http server ... ")
}

func (s *server) Close() {
	var err error
	if !atomic.CompareAndSwapInt32(&s.state, stateRun, stateClosed) {
		log.Error().Msg("server stopped or not running")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), closeTimeout*time.Second)
	defer cancel()

	if err = s.httpServer.Shutdown(ctx); err != nil {
		log.Warn().Err(err).Msg("closing http server error")
	}

	log.Info().Msg("server stopped ...")
}
