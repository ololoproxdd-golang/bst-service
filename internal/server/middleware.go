package server

import (
	"github.com/gorilla/mux"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/middleware"
)

func newHttpMiddleware() []mux.MiddlewareFunc {
	return []mux.MiddlewareFunc{
		middleware.LoggerHTTP,
		middleware.RecoveryHTTP,
	}
}
