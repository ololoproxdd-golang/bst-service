package server

import (
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/middleware"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/service/tree"
	health_storage "gitlab.com/ololoproxdd-golang/bst-service/internal/storage/health"
	tree_storage "gitlab.com/ololoproxdd-golang/bst-service/internal/storage/tree"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/transport"
	"net/http"
	"strconv"
)

func (s *server) getTreeService() tree.Service {
	if s.service.tree != nil {
		return s.service.tree
	}

	s.service.tree = tree.New(s.getTreeStorage())
	s.service.tree = tree.NewMetrics(s.getMonitor())(s.service.tree)

	log.Info().Msg("created tree service ...")
	return s.service.tree
}

func (s *server) getTreeStorage() svc.TreeStorage {
	if s.storage.tree != nil {
		return s.storage.tree
	}

	s.storage.tree = tree_storage.New(s.getDbPostgres())
	s.storage.tree = tree_storage.NewMetrics(s.getMonitor())(s.storage.tree)

	log.Info().Msg("created tree storage ...")
	return s.storage.tree
}

func (s *server) getHealthService() health.Service {
	if s.service.health != nil {
		return s.service.health
	}

	s.service.health = health.New(s.getHealthStorage())
	s.service.health = health.NewMetrics(s.getMonitor())(s.service.health)

	log.Info().Msg("created health service ...")
	return s.service.health
}

func (s *server) getHealthStorage() svc.HealthStorage {
	if s.storage.health != nil {
		return s.storage.health
	}

	s.storage.health = health_storage.New(s.getDbPostgres())
	s.storage.health = health_storage.NewMetrics(s.getMonitor())(s.storage.health)

	log.Info().Msg("created health storage ...")
	return s.storage.health
}

func (s *server) getDbPostgres() *sqlx.DB {
	if s.db != nil {
		return s.db
	}

	nativeDb := stdlib.OpenDB(s.getPostgresConfig())
	db := sqlx.NewDb(nativeDb, "pgx")
	db.SetConnMaxLifetime(0)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	if err := db.Ping(); err != nil {
		log.Fatal().Err(err).Msg("postgres ping error")
		return nil
	}

	log.Info().Msg("created postgres db ...")
	s.db = db
	return s.db
}

func (s *server) getPostgresConfig() pgx.ConnConfig {
	port, err := strconv.ParseUint(s.config.PgPort, 10, 16)
	if err != nil {
		log.Fatal().Err(err).Msg("postgres wrong port")
	}

	return pgx.ConnConfig{
		Host:     s.config.PgHost,
		Port:     uint16(port),
		Database: s.config.PgDb,
		User:     s.config.PgUser,
		Password: s.config.PgPass,

		PreferSimpleProtocol: true,

		Logger:        middleware.NewPgxLogger(),
		LogLevel:      pgx.LogLevelWarn,
		RuntimeParams: map[string]string{"timezone": "UTC"},
	}
}

func (s *server) getTransportHttp() *http.Server {
	if s.httpServer != nil {
		return s.httpServer
	}

	mux := http.NewServeMux()
	mux.Handle("/api/v1/", transport.NewHttpHandler(s.getEndpoints(), newHttpMiddleware()...))
	mux.Handle("/metrics", s.getMonitor().Handler())

	addr := s.config.AppHost + ":" + s.config.AppHttpPort
	s.httpServer = &http.Server{Addr: addr, Handler: mux}

	log.Info().Msgf("created server transport - http %s ... ", s.httpServer.Addr)
	return s.httpServer
}

func (s *server) getEndpoints() *transport.Endpoints {
	if s.endpoints != nil {
		return s.endpoints
	}

	s.endpoints = transport.NewEndpoints(
		s.getHealthService(),
		s.getTreeService(),
	)

	log.Info().Msg("created endpoints ...")
	return s.endpoints
}

func (s *server) getMonitor() *metrics.Monitor {
	if s.monitorCollector != nil {
		return s.monitorCollector
	}

	s.monitorCollector = metrics.NewMonitor(s.config.AppName, s.config.HostName)

	log.Info().Msg("created monitor ...")
	return s.monitorCollector
}
