package tree

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	"gitlab.com/ololoproxdd-golang/bst-service/test"
	"reflect"
	"testing"
)

func TestUnit_Storage_Create(t *testing.T) {
	c := config.LoadConfig()
	s := test.NewTestServer(c)
	conn := s.GetTestDbConnection()
	if err := test.LoadFixtures(conn.DB); err != nil {
		t.Fatal(err.Error())
	}

	storage := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(conn))

	var flagTests = []struct {
		testName string
		ctx      context.Context
		tree     model.Tree
		expID    int64
		expErr   error
	}{
		{
			testName: "successful add tree (with single node) to store",
			ctx:      context.Background(),
			tree: model.Tree{
				Head: &model.Node{
					Value: 12389,
				},
			},
			expID:  10001,
			expErr: nil,
		},
		{
			testName: "successful add tree (with any nodes) to store",
			ctx:      context.Background(),
			tree: model.Tree{
				Head: &model.Node{
					Value:  12389,
					Height: 1,
					Left: &model.Node{
						Value: 12,
					},
					Right: &model.Node{
						Value: 999999,
					},
				},
			},
			expID:  10002,
			expErr: nil,
		},
		{
			testName: "error add empty tree to store",
			ctx:      context.Background(),
			tree:     model.Tree{},
			expID:    0,
			expErr:   errEmptyTree,
		},
	}

	var id int64
	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			id, err = storage.Create(tt.ctx, tt.tree)
			if !reflect.DeepEqual(id, tt.expID) {
				t.Fatal("id != tt.expID")
			}
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}

func TestUnit_Storage_Read(t *testing.T) {
	c := config.LoadConfig()
	s := test.NewTestServer(c)
	conn := s.GetTestDbConnection()
	if err := test.LoadFixtures(conn.DB); err != nil {
		t.Fatal(err.Error())
	}

	storage := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(conn))

	var flagTests = []struct {
		testName string
		ctx      context.Context
		id       int64
		expTree  model.Tree
		expErr   error
	}{
		{
			testName: "successful found tree (with one node) from store",
			ctx:      context.Background(),
			id:       1,
			expTree: model.Tree{
				ID: 1,
				Head: &model.Node{
					Value: 1,
				},
			},
			expErr: nil,
		},
		{
			testName: "successful found tree (with few nodes) from store",
			ctx:      context.Background(),
			id:       2,
			expTree: model.Tree{
				ID: 2,
				Head: &model.Node{
					Value:  12389,
					Height: 1,
					Left: &model.Node{
						Value: 12,
					},
					Right: &model.Node{
						Value: 999999,
					},
				},
			},
			expErr: nil,
		},
		{
			testName: "not found tree from store",
			ctx:      context.Background(),
			id:       999999,
			expTree:  model.Tree{},
			expErr:   nil,
		},
	}

	var tree model.Tree
	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			tree, err = storage.Read(tt.ctx, tt.id)
			if !reflect.DeepEqual(tree, tt.expTree) {
				t.Fatal("tree != tt.expTree")
			}
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}

func TestUnit_Storage_Update(t *testing.T) {
	c := config.LoadConfig()
	s := test.NewTestServer(c)
	conn := s.GetTestDbConnection()
	if err := test.LoadFixtures(conn.DB); err != nil {
		t.Fatal(err.Error())
	}

	storage := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))(New(conn))

	var flagTests = []struct {
		testName string
		ctx      context.Context
		tree     model.Tree
		expErr   error
	}{
		{
			testName: "successful update tree (with one node) in store",
			ctx:      context.Background(),
			tree: model.Tree{
				ID: 2,
				Head: &model.Node{
					Value: 12345,
				},
			},
			expErr: nil,
		},
		{
			testName: "successful update tree (with few nodes) in store",
			ctx:      context.Background(),
			tree: model.Tree{
				ID: 1,
				Head: &model.Node{
					Value:  10,
					Height: 1,
					Left: &model.Node{
						Value: 1,
					},
					Right: &model.Node{
						Value: 100,
					},
				},
			},
			expErr: nil,
		},
		{
			testName: "error update tree (with empty nodes) in store",
			ctx:      context.Background(),
			tree: model.Tree{
				ID:   2,
				Head: nil,
			},
			expErr: errEmptyTree,
		},
		{
			testName: "not updated tree (with unknown id) in store",
			ctx:      context.Background(),
			tree: model.Tree{
				ID: 9999,
				Head: &model.Node{
					Value: 9999,
				},
			},
			expErr: nil,
		},
	}

	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			err = storage.Update(tt.ctx, tt.tree)
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}
