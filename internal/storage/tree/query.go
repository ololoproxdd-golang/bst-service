package tree

const (
	createTree = `insert into structure.tree(value) values ($1) returning id;`
	readTree   = `select id, value from structure.tree where id = $1;`
	updateTree = `update structure.tree set value = $1 where id = $2;`
)
