package tree

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	storage_model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/storage"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
)

const (
	errMarshalTreeText   = "marshal tree to json error"
	errUnmarshalTreeText = "unmarshal tree from json error"
	errAddTreeText       = "add new tree to store error"
	errReadTreeText      = "read tree from store error"
	errUpdateTreeText    = "update tree from store error"
)

var errEmptyTree = errors.New("created tree is empty")

type storage struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) svc.TreeStorage {
	return storage{db}
}

func (s storage) Create(ctx context.Context, t model.Tree) (int64, error) {
	tree := storage_model.Tree{}.ToStorageModel(t)
	if tree.Head == nil {
		return 0, errEmptyTree
	}

	v, err := json.Marshal(tree.Head)
	if err != nil {
		return 0, errors.Wrap(err, errMarshalTreeText)
	}

	var id int64
	err = s.db.QueryRowxContext(ctx, createTree, string(v)).Scan(&id)
	if err != nil {
		return 0, errors.Wrap(err, errAddTreeText)
	}

	return id, nil
}

func (s storage) Read(ctx context.Context, id int64) (model.Tree, error) {
	var tree storage_model.Tree
	err := s.db.QueryRowxContext(ctx, readTree, id).StructScan(&tree)
	if err == sql.ErrNoRows {
		return model.Tree{}, nil
	}
	if err != nil {
		return model.Tree{}, errors.Wrap(err, errReadTreeText)
	}

	err = json.Unmarshal([]byte(tree.Value), &tree.Head)
	if err != nil {
		return model.Tree{}, errors.Wrap(err, errUnmarshalTreeText)
	}

	return tree.ToServiceModel(), nil
}

func (s storage) Update(ctx context.Context, t model.Tree) error {
	tree := storage_model.Tree{}.ToStorageModel(t)
	if tree.Head == nil {
		return errEmptyTree
	}

	v, err := json.Marshal(tree.Head)
	if err != nil {
		return errors.Wrap(err, errMarshalTreeText)
	}

	_, err = s.db.ExecContext(ctx, updateTree, string(v), tree.ID)
	if err != nil {
		return errors.Wrap(err, errUpdateTreeText)
	}

	return nil
}
