package tree

import (
	"context"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
	"time"
)

const (
	methodCreate = "storage/tree/Create"
	methodRead   = "storage/tree/Read"
	methodUpdate = "storage/tree/Update"
)

type metricMiddleware struct {
	svc.TreeStorage
	monitor *metrics.Monitor
}

func NewMetrics(monitor *metrics.Monitor) svc.TreeStorageMiddleware {
	return func(next svc.TreeStorage) svc.TreeStorage {
		return &metricMiddleware{
			TreeStorage: next,
			monitor:     monitor,
		}
	}
}

func (m *metricMiddleware) Create(ctx context.Context, tree model.Tree) (int64, error) {
	defer m.monitor.Metrics.DurationInc(methodCreate, time.Now())
	r, err := m.TreeStorage.Create(ctx, tree)
	m.monitor.Metrics.CountInc(methodCreate, err)
	return r, err
}

func (m *metricMiddleware) Read(ctx context.Context, id int64) (model.Tree, error) {
	defer m.monitor.Metrics.DurationInc(methodRead, time.Now())
	r, err := m.TreeStorage.Read(ctx, id)
	m.monitor.Metrics.CountInc(methodRead, err)
	return r, err
}

func (m *metricMiddleware) Update(ctx context.Context, tree model.Tree) error {
	defer m.monitor.Metrics.DurationInc(methodUpdate, time.Now())
	err := m.TreeStorage.Update(ctx, tree)
	m.monitor.Metrics.CountInc(methodUpdate, err)
	return err
}
