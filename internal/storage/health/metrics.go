package health

import (
	"context"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
	"time"
)

const methodCheck = "storage/health/Check"

type metricMiddleware struct {
	svc.HealthStorage
	monitor *metrics.Monitor
}

func NewMetrics(monitor *metrics.Monitor) svc.HealthStorageMiddleware {
	return func(next svc.HealthStorage) svc.HealthStorage {
		return &metricMiddleware{
			HealthStorage: next,
			monitor:       monitor,
		}
	}
}

func (m *metricMiddleware) Check(ctx context.Context) error {
	defer m.monitor.Metrics.DurationInc(methodCheck, time.Now())
	err := m.HealthStorage.Check(ctx)
	m.monitor.Metrics.CountInc(methodCheck, err)
	return err
}
