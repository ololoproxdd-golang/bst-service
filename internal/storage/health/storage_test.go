package health

import (
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/metrics"
	"gitlab.com/ololoproxdd-golang/bst-service/test"
	"testing"
)

func TestUnit_Storage_Check(t *testing.T) {
	c := config.LoadConfig()
	s := test.NewTestServer(c)
	metricsWrap := NewMetrics(metrics.NewMonitor(c.AppName, c.HostName))

	var flagTests = []struct {
		testName string
		conn     *sqlx.DB
		ctx      context.Context
		expErr   error
	}{
		{
			testName: "successful check store",
			conn:     s.GetTestDbConnection(),
			ctx:      context.Background(),
			expErr:   nil,
		},
		{
			testName: "error check store (bad connection)",
			conn:     sqlx.MustOpen("pgx", "bad_connection_string"),
			ctx:      context.Background(),
			expErr:   errPingDb,
		},
	}

	var err error
	for _, tt := range flagTests {
		t.Run(tt.testName, func(t *testing.T) {
			storage := metricsWrap(New(tt.conn))
			err = storage.Check(tt.ctx)
			if !errors.Is(err, tt.expErr) {
				t.Fatal("err != tt.expErr")
			}
		})
	}
}
