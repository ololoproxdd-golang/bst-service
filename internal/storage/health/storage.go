package health

import (
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	svc "gitlab.com/ololoproxdd-golang/bst-service/internal/service"
)

var errPingDb = errors.New("ping db error")

type storage struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) svc.HealthStorage {
	return storage{db}
}

func (s storage) Check(ctx context.Context) error {
	err := s.db.PingContext(ctx)
	if err != nil {
		log.Error().Err(err).Msg(errPingDb.Error())
		return errPingDb
	}

	return nil
}
