package middleware

import (
	"bytes"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type statusWriter struct {
	http.ResponseWriter
	status int
	length int
}

func (w *statusWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *statusWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = http.StatusOK
	}

	n, err := w.ResponseWriter.Write(b)
	w.length += n

	return n, err
}

func LoggerHTTP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		defer req.Body.Close()

		var body interface{}
		b := bytes.NewBuffer(make([]byte, 0))
		err := json.NewDecoder(io.TeeReader(req.Body, b)).Decode(&body)

		switch {
		case err == io.EOF:
			break
		case err != nil:
			log.Error().Err(err).Msg("decode body error")
		}

		req.Body = ioutil.NopCloser(b)
		start := time.Now()
		sw := &statusWriter{ResponseWriter: w}
		next.ServeHTTP(sw, req)
		duration := time.Since(start)

		log.Info().
			Str("Method", req.Method+": "+req.URL.Path).
			Str("Duration", duration.String()).
			Msgf("Status: %d", sw.status)
	})
}
