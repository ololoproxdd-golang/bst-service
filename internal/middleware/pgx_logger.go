package middleware

import (
	"github.com/jackc/pgx"
	"github.com/rs/zerolog/log"
)

const pgxLoggerTemplate = "[pgx] msg: %s data:%v"

type pgxLogger struct {
}

func NewPgxLogger() pgx.Logger {
	l := &pgxLogger{}
	return l
}

func (l *pgxLogger) Log(level pgx.LogLevel, msg string, data map[string]interface{}) {
	switch level {
	case pgx.LogLevelNone:
	case pgx.LogLevelTrace:
	case pgx.LogLevelDebug:
	case pgx.LogLevelInfo:
		log.Debug().Msgf(pgxLoggerTemplate, msg, data)
	case pgx.LogLevelWarn:
		log.Warn().Msgf(pgxLoggerTemplate, msg, data)
	case pgx.LogLevelError:
		log.Error().Msgf(pgxLoggerTemplate, msg, data)
	}
}
