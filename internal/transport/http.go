package transport

import (
	"context"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	"net/http"
	"net/url"
	"strconv"
)

func NewHttpHandler(endpoints *Endpoints, middleware ...mux.MiddlewareFunc) http.Handler {
	r := mux.NewRouter()
	r.Use(middleware...)

	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(NewErrorHandler("http")),
		kithttp.ServerErrorEncoder(encodeError),
	}

	pingHandler := kithttp.NewServer(
		endpoints.Ping,
		noDecodeRequest,
		encodeResponse,
		opts...,
	)

	healthHandler := kithttp.NewServer(
		endpoints.HealthCheck,
		noDecodeRequest,
		encodeResponse,
		opts...,
	)

	createTreeHandler := kithttp.NewServer(
		endpoints.CreateTree,
		decodeCreateRequest,
		encodeResponse,
		opts...,
	)

	readTreeHandler := kithttp.NewServer(
		endpoints.ReadTree,
		decodeReadRequest,
		encodeResponse,
		opts...,
	)

	insertTreeValueHandler := kithttp.NewServer(
		endpoints.InsertTreeValue,
		decodeInsertRequest,
		encodeResponse,
		opts...,
	)

	deleteTreeValueHandler := kithttp.NewServer(
		endpoints.DeleteTreeValue,
		decodeDeleteRequest,
		encodeResponse,
		opts...,
	)

	r.Handle("/api/v1/ping", pingHandler).Methods(http.MethodGet)
	r.Handle("/api/v1/health", healthHandler).Methods(http.MethodGet)

	r.Handle("/api/v1/tree/{id}/{value}", insertTreeValueHandler).Methods(http.MethodPost)
	r.Handle("/api/v1/tree", createTreeHandler).Methods(http.MethodPost)
	r.Handle("/api/v1/tree/{id}", readTreeHandler).Methods(http.MethodGet)
	r.Handle("/api/v1/tree/{id}/{value}", deleteTreeValueHandler).Methods(http.MethodDelete)
	return r
}

func noDecodeRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func decodeCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var values []int64
	if err := json.NewDecoder(r.Body).Decode(&values); err != nil {
		return nil, err
	}

	return model.CreateRequest{Values: values}, nil
}

func decodeReadRequest(_ context.Context, req *http.Request) (interface{}, error) {
	vars := mux.Vars(req)
	v, ok := vars["id"]
	if !ok {
		return 0, errors.New("is not exists value 'id'")
	}

	id, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, "decode path param error")
	}

	values, err := url.ParseQuery(req.URL.RawQuery)
	if err != nil {
		return nil, errors.Wrap(err, "parse query params error")
	}

	var value *int64
	for k, v := range values {
		switch k {
		case "val":
			if len(v) > 0 {
				val, err := strconv.ParseInt(v[0], 10, 64)
				if err != nil {
					return nil, errors.Wrap(err, "parse search value error")
				}
				value = &val
			}
		}
	}

	return model.ReadRequest{
		IDRequest: model.IDRequest{ID: id},
		Value:     value,
	}, nil
}

func decodeInsertRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	r, err := decodeIDValuePathRequest(ctx, req)
	return model.InsertRequest(r), err
}

func decodeDeleteRequest(ctx context.Context, req *http.Request) (interface{}, error) {
	r, err := decodeIDValuePathRequest(ctx, req)
	return model.DeleteRequest(r), err
}

func decodeIDValuePathRequest(_ context.Context, req *http.Request) (model.IDValueRequest, error) {
	vars := mux.Vars(req)
	v, ok := vars["id"]
	if !ok {
		return model.IDValueRequest{}, errors.New("is not exists value 'id'")
	}

	id, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return model.IDValueRequest{}, errors.Wrap(err, "decode path param error")
	}

	v, ok = vars["value"]
	if !ok {
		return model.IDValueRequest{}, errors.New("is not exists value 'value'")
	}

	value, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return model.IDValueRequest{}, errors.Wrap(err, "decode path param error")
	}

	return model.IDValueRequest{
		IDRequest:    model.IDRequest{ID: id},
		ValueRequest: model.ValueRequest{Value: value},
	}, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}

	w.Header().Set("content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
