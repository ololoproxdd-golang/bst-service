package transport

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"net/http"
)

var errNotFound = errors.New("tree not found")

type errorer interface {
	error() error
}

type errorHandler struct {
	transport string
}

func NewErrorHandler(transport string) *errorHandler {
	return &errorHandler{transport: transport}
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("content-Type", "application/json; charset=utf-8")

	var code int
	switch err {
	case errNotFound:
		code = http.StatusNotFound
	default:
		code = http.StatusBadRequest
	}
	w.WriteHeader(code)

	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})

	if err != nil {
		log.Error().Err(err).Msg("encode to json failed in encodeError")
	}
}

func (h *errorHandler) Handle(_ context.Context, err error) {
	log.Error().Err(err).Msgf("in '%s' transport", h.transport)
}
