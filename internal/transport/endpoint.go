package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/service"
	transport_model "gitlab.com/ololoproxdd-golang/bst-service/internal/model/transport"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/service/tree"
)

var (
	errCreateTreeEndpoint = errors.New("makeCreateTreeEndpoint not type of model.CreateRequest")
	errReadTreeEndpoint   = errors.New("makeReadTreeEndpoint not type of model.ReadRequest")
	errInsertTreeEndpoint = errors.New("makeInsertTreeValueEndpoint not type of model.InsertRequest")
	errDeleteTreeEndpoint = errors.New("makeDeleteTreeValueEndpoint not type of model.DeleteRequest")
)

type Endpoints struct {
	Ping        endpoint.Endpoint
	HealthCheck endpoint.Endpoint

	CreateTree      endpoint.Endpoint
	ReadTree        endpoint.Endpoint
	InsertTreeValue endpoint.Endpoint
	DeleteTreeValue endpoint.Endpoint
}

func NewEndpoints(h health.Service, t tree.Service) *Endpoints {
	return &Endpoints{
		Ping:            makePingEndpoint(h),
		HealthCheck:     makeHealthEndpoint(h),
		CreateTree:      makeCreateTreeEndpoint(t),
		ReadTree:        makeReadTreeEndpoint(t),
		InsertTreeValue: makeInsertTreeValueEndpoint(t),
		DeleteTreeValue: makeDeleteTreeValueEndpoint(t),
	}
}

func makePingEndpoint(h health.Service) endpoint.Endpoint {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		return h.Ping(ctx), nil
	}
}

func makeHealthEndpoint(h health.Service) endpoint.Endpoint {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		return h.Check(ctx)
	}
}

func makeCreateTreeEndpoint(treeService tree.Service) endpoint.Endpoint {
	return func(ctx context.Context, r interface{}) (interface{}, error) {
		req, ok := r.(model.CreateRequest)
		if !ok {
			log.Error().
				Err(errCreateTreeEndpoint).
				Interface("request", r).
				Msg("decode request error")
			return nil, errCreateTreeEndpoint
		}

		tr, err := treeService.Create(ctx, req)
		if err != nil {
			return nil, err
		}

		return transport_model.Tree{}.ToTransportModel(tr), nil
	}
}

func makeReadTreeEndpoint(treeService tree.Service) endpoint.Endpoint {
	return func(ctx context.Context, r interface{}) (interface{}, error) {
		req, ok := r.(model.ReadRequest)
		if !ok {
			log.Error().
				Err(errReadTreeEndpoint).
				Interface("request", r).
				Msg("decode request error")
			return nil, errReadTreeEndpoint
		}

		tr, found, err := treeService.Read(ctx, req)
		if err != nil {
			return nil, err
		}

		transportTree := transport_model.Tree{}.ToTransportModel(tr)
		if transportTree.ID == 0 {
			return nil, errNotFound
		}

		return transport_model.ReadResponse{
			Tree:    transportTree,
			IsFound: found,
		}, nil
	}
}

func makeInsertTreeValueEndpoint(treeService tree.Service) endpoint.Endpoint {
	return func(ctx context.Context, r interface{}) (interface{}, error) {
		req, ok := r.(model.InsertRequest)
		if !ok {
			log.Error().
				Err(errInsertTreeEndpoint).
				Interface("request", r).
				Msg("decode request error")
			return nil, errInsertTreeEndpoint
		}

		tr, err := treeService.InsertValue(ctx, req)
		if err != nil {
			return nil, err
		}

		transportTree := transport_model.Tree{}.ToTransportModel(tr)
		if transportTree.ID == 0 {
			return nil, errNotFound
		}

		return transportTree, nil
	}
}

func makeDeleteTreeValueEndpoint(treeService tree.Service) endpoint.Endpoint {
	return func(ctx context.Context, r interface{}) (interface{}, error) {
		req, ok := r.(model.DeleteRequest)
		if !ok {
			log.Error().
				Err(errDeleteTreeEndpoint).
				Interface("request", r).
				Msg("decode request error")
			return nil, errDeleteTreeEndpoint
		}

		tr, deleted, err := treeService.DeleteValue(ctx, req)
		if err != nil {
			return nil, err
		}

		transportTree := transport_model.Tree{}.ToTransportModel(tr)
		if transportTree.ID == 0 {
			return nil, errNotFound
		}

		return transport_model.DeleteResponse{
			Tree:      transportTree,
			IsDeleted: deleted,
		}, nil
	}
}
