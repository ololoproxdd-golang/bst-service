package transport

import (
	"context"
	"github.com/gorilla/mux"
	"github.com/nsf/jsondiff"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/config"
	"gitlab.com/ololoproxdd-golang/bst-service/internal/service/tree"
	tree_storage "gitlab.com/ololoproxdd-golang/bst-service/internal/storage/tree"
	"gitlab.com/ololoproxdd-golang/bst-service/test"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestFunctional_CreateTree(t *testing.T) {
	ctx, w, endpoints := initialize(t)

	handler := func(w http.ResponseWriter, r *http.Request) {
		req, err := decodeCreateRequest(ctx, r)
		if err != nil {
			t.Fatal("error decode request", err)
		}

		resp, err := endpoints.CreateTree(ctx, req)
		if err != nil {
			t.Fatal("error work endpoint", err)
		}

		err = encodeResponse(ctx, w, resp)
		if err != nil {
			t.Fatal("error encode response", err)
		}
	}

	bodyReader := strings.NewReader(`[1,2,3,7]`)
	req := httptest.NewRequest(http.MethodPost, "http://host/api/v1/tree", bodyReader)
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	expBody := `
{
    "id": 10001,
    "data": {
        "value": 2,
        "left": {
            "value": 1
        },
        "right": {
            "value": 3,
            "right": {
                "value": 7
            }
        }
    }
}`
	opts := jsondiff.DefaultJSONOptions()
	diff, _ := jsondiff.Compare(body, []byte(expBody), &opts)
	if diff != jsondiff.FullMatch {
		t.Fatal("body != expBody")
	}
}

func TestFunctional_ReadTree(t *testing.T) {
	ctx, w, endpoints := initialize(t)

	handler := func(w http.ResponseWriter, r *http.Request) {
		req, err := decodeReadRequest(ctx, r)
		if err != nil {
			t.Fatal("error decode request", err)
		}

		resp, err := endpoints.ReadTree(ctx, req)
		if err != nil {
			t.Fatal("error work endpoint", err)
		}

		err = encodeResponse(ctx, w, resp)
		if err != nil {
			t.Fatal("error encode response", err)
		}
	}

	req := httptest.NewRequest(http.MethodPost, "http://host/api/v1/tree/2?val=12389", nil)
	req = mux.SetURLVars(req, map[string]string{
		"id": "2",
	})
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	expBody := `
{
    "tree": {
        "id": 2,
        "data": {
            "value": 12389,
            "left": {
                "value": 12
            },
            "right": {
                "value": 999999
            }
        }
    },
    "is_found": true
}`
	opts := jsondiff.DefaultJSONOptions()
	diff, _ := jsondiff.Compare(body, []byte(expBody), &opts)
	if diff != jsondiff.FullMatch {
		t.Fatal("body != expBody")
	}
}

func TestFunctional_InsertTreeValue(t *testing.T) {
	ctx, w, endpoints := initialize(t)

	handler := func(w http.ResponseWriter, r *http.Request) {
		req, err := decodeInsertRequest(ctx, r)
		if err != nil {
			t.Fatal("error decode request", err)
		}

		resp, err := endpoints.InsertTreeValue(ctx, req)
		if err != nil {
			t.Fatal("error work endpoint", err)
		}

		err = encodeResponse(ctx, w, resp)
		if err != nil {
			t.Fatal("error encode response", err)
		}
	}

	req := httptest.NewRequest(http.MethodPost, "http://host/api/v1/tree/2/9090", nil)
	req = mux.SetURLVars(req, map[string]string{
		"id":    "2",
		"value": "9090",
	})
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	expBody := `
{
    "id": 2,
    "data": {
        "value": 12389,
        "left": {
            "value": 12,
            "right": {
                "value": 9090
            }
        },
        "right": {
            "value": 999999
        }
    }
}`
	opts := jsondiff.DefaultJSONOptions()
	diff, _ := jsondiff.Compare(body, []byte(expBody), &opts)
	if diff != jsondiff.FullMatch {
		t.Fatal("body != expBody")
	}
}

func TestFunctional_DeleteTreeValue(t *testing.T) {
	ctx, w, endpoints := initialize(t)

	handler := func(w http.ResponseWriter, r *http.Request) {
		req, err := decodeDeleteRequest(ctx, r)
		if err != nil {
			t.Fatal("error decode request", err)
		}

		resp, err := endpoints.DeleteTreeValue(ctx, req)
		if err != nil {
			t.Fatal("error work endpoint", err)
		}

		err = encodeResponse(ctx, w, resp)
		if err != nil {
			t.Fatal("error encode response", err)
		}
	}

	req := httptest.NewRequest(http.MethodDelete, "http://host/api/v1/tree/2/999999", nil)
	req = mux.SetURLVars(req, map[string]string{
		"id":    "2",
		"value": "999999",
	})
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	expBody := `
{
    "tree": {
        "id": 2,
        "data": {
            "value": 12389,
            "left": {
                "value": 12
            }
        }
    },
    "is_deleted": true
}`
	opts := jsondiff.DefaultJSONOptions()
	diff, _ := jsondiff.Compare(body, []byte(expBody), &opts)
	if diff != jsondiff.FullMatch {
		t.Fatal("body != expBody")
	}
}

func initialize(t *testing.T) (context.Context, *httptest.ResponseRecorder, *Endpoints) {
	s := test.NewTestServer(config.LoadConfig())
	conn := s.GetTestDbConnection()
	if err := test.LoadFixtures(conn.DB); err != nil {
		t.Fatal(err.Error())
	}

	store := tree_storage.New(conn)
	svc := tree.New(store)

	ctx := context.Background()
	w := httptest.NewRecorder()
	endpoints := NewEndpoints(nil, svc)
	return ctx, w, endpoints
}
